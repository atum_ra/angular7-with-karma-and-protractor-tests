import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { TableModule } from 'ngx-easy-table';

import { InMemoryWebApiModule } from "angular-in-memory-web-api";  
import { BackendService } from "./backend.service";

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableComponent } from './contact-list/table.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { CreateForm } from './contact-list/create-form/create-form.component';
import { Login } from './contact-list/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    ContactDetailComponent,
    CreateForm,
    Login
  ],
  imports: [
    BrowserModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
