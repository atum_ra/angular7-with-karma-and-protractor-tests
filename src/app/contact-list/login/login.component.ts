import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class Login implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    ) {}
  errors: string[] = [];

  onSubmit(value: any) {
      const {username, password} = value;

      this.authService.logIn(username, password).subscribe(
        data => this.router.navigateByUrl('/'),
        httpError => {
          console.log("login", httpError);
          const apiErrorObject = httpError.body.error;
          this.errors=[apiErrorObject]; 

        }
      )
  }

  ngOnInit() {
  }

}
